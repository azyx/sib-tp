$setup_proxy = <<SCRIPT
set -o nounset -o pipefail -o errexit

readonly PROVISION_HTTP_PROXY="#{ENV['HTTP_PROXY']}"
readonly PROVISION_HTTPS_PROXY="#{ENV['HTTPS_PROXY']}"

### Configure proxy
if [ -n "${PROVISION_HTTP_PROXY}" ] ; then
    tee "/etc/environment" > "/dev/null" <<EOF
http_proxy="${PROVISION_HTTP_PROXY}"
EOF

    if [ -n "${PROVISION_HTTPS_PROXY}" ] ; then
        tee -a "/etc/environment" > "/dev/null" <<EOF
https_proxy="${PROVISION_HTTPS_PROXY}"
EOF
   fi
fi
SCRIPT

$setup_sshkey = <<SCRIPT
set -o nounset -o pipefail -o errexit

readonly PROVISION_HTTP_PROXY="#{ENV['HTTP_PROXY']}"
readonly PROVISION_HTTPS_PROXY="#{ENV['HTTPS_PROXY']}"

### Download Vagrant private key
if ! type wget > /dev/null ; then
    sudo yum install -y wget
fi
http_proxy=${PROVISION_HTTP_PROXY} \
https_proxy=${PROVISION_HTTPS_PROXY} \
wget -O $HOME/.ssh/id_rsa https://raw.githubusercontent.com/hashicorp/vagrant/master/keys/vagrant

chmod 600 $HOME/.ssh/id_rsa
SCRIPT

Vagrant.configure("2") do |config|

  # use same private key on all machines
  config.ssh.insert_key = false

  config.vm.synced_folder ".", "/vagrant", disabled: true

  # configure proxy on all VMs if defined
  config.vm.provision "proxy", type: "shell", inline: $setup_proxy

  # Ansible controller
  if ENV['ORSYS'] == "true"
    # Don't create or list 'controller' VM because your host is the controller
  else
    config.vm.define "controller" do |vm1|
      vm1.vm.box = ENV['VAGRANT_BOX'] || 'debian/contrib-buster64'
      vm1.vm.hostname = "controller"
      vm1.vm.network :private_network, ip: "10.42.0.10"
      vm1.vm.provider "virtualbox" do |v|
        v.memory = "2048"
        v.cpus = "1"
      end

      # use VB synced folders on Windows: need to have guest additions in VM
      # use rsync on other platforms
      if Vagrant::Util::Platform.windows? then
        vm1.vm.synced_folder "codes", "/home/vagrant/codes"
      else
        vm1.vm.synced_folder "codes", "/home/vagrant/codes", type: "rsync"
      end

      # install private key on controller
      vm1.vm.provision "sshkey", type: "shell", inline: $setup_sshkey, privileged: false

      # fake to create automatically inventory file and test student role
      vm1.vm.provision "ansible", type: "ansible", run:"never" do |ansible|
        ansible.playbook = "site.yml"
        ansible.groups = {
          "students" => [ "controller" ]
        }
      end
    end
  end

  # node CentOS
  config.vm.define "node1-c7" do |vm2|
    if ENV['ORSYS'] == "true"
      vm2.vm.box = "centos7"
      vm2.vm.box_url = "file://centos-7.box"
    else
      vm2.vm.box = "centos/7"
    end
    vm2.vm.hostname = "node1-c7"
    vm2.vm.network :private_network, ip: "10.42.0.11"
    vm2.vm.provider "virtualbox" do |v|
      v.memory = "256"
      v.cpus = "1"
    end
  end

  # node Debian
  config.vm.define "node2-deb" do |vm3|
    if ENV['ORSYS'] == "true"
      vm3.vm.box = "buster64"
      vm3.vm.box_url = "file://debian-buster.box"
    else
      vm3.vm.box = "debian/buster64"
    end
    vm3.vm.hostname = "node2-deb"
    vm3.vm.network :private_network, ip: "10.42.0.12"
    vm3.vm.provider "virtualbox" do |v|
      v.memory = "256"
      v.cpus = "1"
    end
  end

  # ansible-tower, no private network because VM has already one
  config.vm.define "ansible-tower", autostart: false do |vm4|
    if ENV['ORSYS'] == "true"
      vm4.vm.box = "ansible-tower"
      vm4.vm.box_url = "file://ansible-tower.box"
    else
      vm4.vm.box = "ansible/tower"
    end    
    vm4.vm.hostname = "ansible-tower"
    vm4.vm.provider "virtualbox" do |v|
      v.memory = "2048"
      v.cpus = "1"
    end
  end
end
