#!/bin/bash
set -o nounset -o pipefail -o errexit

ansible-galaxy install geerlingguy.postfix

# to install role in a specific directory which is not part of your DEFAULT_ROLES_PATH
ansible-galaxy install geerlingguy.postfix -p roles

ansible-playbook site.yml
