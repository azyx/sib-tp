#!/bin/bash
set -o nounset -o pipefail -o errexit

# to verify roles' job
ansible web -a "systemctl is-active postfix"
ansible web -a "grep ipv4 /etc/postfix/main.cf"

# download roles using a requirement file
ansible-galaxy install -r requirements.yml
