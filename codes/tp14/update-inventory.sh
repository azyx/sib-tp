#!/bin/bash
set -o nounset -o pipefail -o errexit

rm $HOME/ansible/inventory
ln -s $HOME/codes/tp14/inventory $HOME/ansible/inventory

