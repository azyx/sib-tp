#!/bin/bash
set -o nounset -o pipefail -o errexit

mkdir -v -p $HOME/ansible
ln -s $HOME/codes/inventory $HOME/ansible/inventory
