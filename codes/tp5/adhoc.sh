#!/bin/bash
set -o nounset -o pipefail -o errexit

ansible web --module-name ping
ansible web --module-name setup
# become to have root rights
ansible node1-c7 --become --module-name yum --args "name=httpd state=latest"
ansible node2-deb --become --module-name apt --args "name=apache2 state=latest"

# we can't use debug module because ansible command doesn't gather facts
ansible node2-deb --module-name setup --args "filter=ansible_distribution_release"
