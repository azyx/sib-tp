#!/bin/bash
set -o nounset -o pipefail -o errexit

ansible web --become -m package -a "name=* state=latest"
ansible web --become -m package -a "name=tree state=present"
ansible web --become -m reboot
