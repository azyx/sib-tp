#!/bin/bash
set -o nounset -o pipefail -o errexit

sudo apt install gnupg2 -y
sudo echo "deb http://ppa.launchpad.net/ansible/ansible/ubuntu trusty main"  > /etc/apt/sources.list.d/ansible.list

sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 93C4A3FD7BB9C367
sudo apt update
sudo apt install ansible -y
