#!/bin/bash
set -o nounset -o pipefail -o errexit

sudo apt install vim -y
echo -e "set softtabstop=2 expandtab shiftwidth=2 smarttab autoindent\nsyntax on" > ~/.vimrc
