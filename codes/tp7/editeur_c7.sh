#!/bin/bash
set -o nounset -o pipefail -o errexit

sudo yum install vim vim-enhanced -y
echo -e "set softtabstop=2 expandtab shiftwidth=2 smarttab autoindent\nsyntax on" > ~/.vimrc
