#!/bin/bash
set -o nounset -o pipefail -o errexit

cp $HOME/codes/tp4/ansible.cfg $HOME/ansible/ansible.cfg
cd $HOME/ansible
ansible --version
ansible-config dump --only-changed
