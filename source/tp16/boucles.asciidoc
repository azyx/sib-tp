== TP16 : boucles

ifeval::["{public}" == "apprenant"]
Temps estimé : 10 minutes

*Objectif* : utiliser des boucles pour gérer plusieurs domaines

=== Outil(s) à mettre en œuvre

Boucles

=== Tâche(s) à réaliser et résultat attendu

. Transformer `apache_domain` en `apache_domains` (pluriel) 
. Adapter votre inventaire à ce changement
. Adapter le code de votre rôle à ce changement

.Résultat attendu

Inventaire identique à link:{sib_tp_codes_dir}/tp16/inventory/{env}/host_vars/node1-c7/apache.yml[celui-ci]

// === Documentation

=== Exercice(s) complémentaire(s) (optionnel)

. Utiliser une boucle `until` et le module `uri` pour vérifier l'accès aux pages HTML
endif::[]


// Corrigé
ifeval::["{public}" == "formateur"]
include::s_boucles.asciidoc[]
endif::[]
