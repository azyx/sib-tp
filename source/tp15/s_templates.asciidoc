=== Corrigé

.Fichier [filename]`roles/apache/templates/index.html.j2`
[source,html]
----
include::{codedir}/tp15/roles/apache/templates/index.html.j2[]
----

.Fichier [filename]`roles/apache/templates/vhost.conf.j2`
[source]
----
include::{codedir}/tp15/roles/apache/templates/vhost.conf.j2[]
----

.Fichier [filename]`roles/apache/tasks/main.yml`
[source,yaml]
----
include::{codedir}/tp15/roles/apache/tasks/main.yml[]
----

// insertion code directement
.Surcharge d'`apache_domain` sur la ligne de commande
ifeval::["{env}" == "orsys"]
[source,bash]
----
ansible-playbook site.yml -e "apache_domain=foofoo.fr"
----
endif::[]

ifeval::["{env}" == "intra"]
[source,bash]
----
ansible-playbook site.yml -e "apache_domain=www.10.42.0.11.xip.io"
----
endif::[]

// === Corrigé complémentaire
