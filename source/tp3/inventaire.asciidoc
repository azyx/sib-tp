== TP3 : inventaire simple

ifeval::["{public}" == "apprenant"]
Temps estimé : 10 minutes

*Objectif* : mettre au point un fichier d'inventaire simple

=== Outil(s) à mettre en œuvre

Éditeur de texte, création de répertoire/fichier, [command]`ansible-inventory`

=== Tâche(s) à réaliser et résultat attendu

. Créer le répertoire `{ansible_dir}`
. Créer le répertoire `inventory` dans `{ansible_dir}` avec un fichier d'inventaire simple (`hosts`) contenant :
.. un groupe `web` contenant les deux machines (`node*`) du TP
.. un groupe `paris` contenant uniquement `{c7}`
ifeval::["{env}" == "intra"]
. Définir les variables `ansible_host` pour chaque machine
. Définir la variable suivante pour le groupe `web` :
[source,ini]
----
ansible_user=vagrant
----
endif::[]

ifeval::["{env}" == "orsys"]
. Définir les variables suivantes pour le groupe `web` :
[source,ini]
----
ansible_user=vagrant
ansible_ssh_private_key_file=~/.vagrant.d/insecure_private_key
----
endif::[]


.Résultat attendu
ifeval::["{env}" == "intra"]
[source,bash]
----
$ ~ ansible-inventory -i $HOME/ansible/inventory --graph --vars
@all:
  |--@paris:
  |  |--node1-c7
  |  |  |--{ansible_user = vagrant}
  |--@ungrouped:
  |--@web:
  |  |--node1-c7
  |  |  |--{ansible_user = vagrant}
  |  |--node2-deb
  |  |  |--{ansible_user = vagrant}
  |  |--{ansible_user = vagrant}
----
endif::[]

ifeval::["{env}" == "orsys"]
[source,bash]
----
@all:
  |--@paris:
  |  |--node1-c7
  |  |  |--{ansible_ssh_private_key_file = ~/.vagrant.d/insecure_private_key}
  |  |  |--{ansible_user = vagrant}
  |--@ungrouped:
  |--@web:
  |  |--node1-c7
  |  |  |--{ansible_ssh_private_key_file = ~/.vagrant.d/insecure_private_key}
  |  |  |--{ansible_user = vagrant}
  |  |--node2-deb
  |  |  |--{ansible_ssh_private_key_file = ~/.vagrant.d/insecure_private_key}
  |  |  |--{ansible_user = vagrant}
  |  |--{ansible_ssh_private_key_file = ~/.vagrant.d/insecure_private_key}
  |  |--{ansible_user = vagrant}
----
endif::[]
=== Documentation

* https://docs.ansible.com/ansible/latest/reference_appendices/special_variables.html#connection-variables[Variables de connexion]

=== Exercice(s) complémentaire(s) (optionnel)

* Exporter l'inventaire au format YAML
endif::[]

// Corrigé
ifeval::["{public}" == "formateur"]
include::s_inventaire.asciidoc[]
endif::[]
