=== Corrigé

==== Configurer son éditeur de texte

.Configuration éditeur sous CentOS
[source,bash]
----
include::{codedir}/tp7/editeur_c7.sh[]
----

.Configuration éditeur sous Debian
[source,bash]
----
include::{codedir}/tp7/editeur_deb.sh[]
----
