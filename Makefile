.PHONY: html
html: formateur apprenant orsys/formateur orsys/apprenant cv/formateur cv/apprenant

# génère formateur.html (uniquement les corrections)
.PHONY: formateur
formateur:
	/usr/bin/asciidoctor --failure-level=ERROR \
	-n \
	-a public=$@ \
	-a env=intra \
	apprenant/master.asciidoc \
	-D public \
	-o $@.html

# génère apprenant.html (tp sans corrections)
.PHONY: apprenant
apprenant:
	/usr/bin/asciidoctor --failure-level=ERROR \
	-n \
	-a public=$@ \
	-a env=intra \
	apprenant/master.asciidoc \
	-D public \
	-o $@.html
	ln -f -s $@.html public/index.html

# génère orsys/formateur.html (uniquement les corrections)
.PHONY: orsys/formateur
orsys/formateur:
	/usr/bin/asciidoctor --failure-level=ERROR \
	-n \
	-a public=formateur \
	-a env=orsys \
	apprenant/master.asciidoc \
	-D public \
	-o $@.html

# génère orsys/apprenant.html (tp sans corrections)
.PHONY: orsys/apprenant
orsys/apprenant:
	/usr/bin/asciidoctor --failure-level=ERROR \
	-n \
	-a public=apprenant \
	-a env=orsys \
	apprenant/master.asciidoc \
	-D public \
	-o $@.html
	ln -f -s apprenant.html public/orsys/index.html

# génère cv/formateur.html
.PHONY: cv/formateur
cv/formateur:
	/usr/bin/asciidoctor --failure-level=ERROR \
	-n \
	-a public=formateur \
	-a env=intra \
	-a local_user=centos \
	-a dns_domain1_node1=IP_PUBLIQUE_NODE1.xip.io \
	-a dns_domain2_node1=www.IP_PUBLIQUE_NODE1.xip.io \
	-a dns_domain1_node2=IP_PUBLIQUE_NODE2.xip.io \
	-a dns_domain2_node2=www.IP_PUBLIQUE_NODE2.xip.io \
	apprenant/master.asciidoc \
	-D public \
	-o $@.html

# génère cv/apprenant.html
.PHONY: cv/apprenant
cv/apprenant:
	/usr/bin/asciidoctor --failure-level=ERROR \
	-n \
	-a public=apprenant \
	-a env=intra \
	-a local_user=centos \
	-a dns_domain1_node1=IP_PUBLIQUE_NODE1.xip.io \
	-a dns_domain2_node1=www.IP_PUBLIQUE_NODE1.xip.io \
	-a dns_domain1_node2=IP_PUBLIQUE_NODE2.xip.io \
	-a dns_domain2_node2=www.IP_PUBLIQUE_NODE2.xip.io \
	apprenant/master.asciidoc \
	-D public \
	-o $@.html
	ln -f -s apprenant.html public/cv/index.html
